﻿using FileViewer.Services.FilePagination;
using FileViewer.Services.FilePagination.Interfaces;
using System;
using System.Threading.Tasks;

namespace FileViewer
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("######################## Teste Banco Modal #############################\n");
            Console.WriteLine("######################## Juanir Rodrigues ##############################");
            Console.WriteLine("###################### Analista de Sistemas ############################");
            Console.WriteLine("################# .Net Core 2.1 - Console Application  #################\n");
            Console.WriteLine("Digite o caminho do arquivo:");

            var path = Console.ReadLine();
            
            IFilePaginationService filePaginationService = new FilePaginationService(path);

            await filePaginationService.WaitPressKey();
        }
    }
}
