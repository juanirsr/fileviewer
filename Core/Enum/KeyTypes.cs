﻿namespace FileViewer.Core.Enum
{
    public enum KeyTypes
    {
        DownArrow,
        UpArrow,
        PageUp,
        PageDown,
        Line
    }
}
