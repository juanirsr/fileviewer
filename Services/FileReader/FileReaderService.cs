﻿using FileViewer.Services.FileReader.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace FileViewer.Services.FileReader
{
    public class FileReaderService : IFileReaderService
    {
        const int _total = 100;
        const int _totalPage = 11;
       
        public string FilePath { get; set; }
        public bool EndOfFile { get; set; }

        public async Task<Dictionary<int, string>> GetBuffer(int offset = 0)
        {
            var buffer = new Dictionary<int, string>();
            
            if (File.Exists(FilePath))
            {
                try
                {
                    using (StreamReader sr = new StreamReader(FilePath))
                    {
                        string line;
                        var lineNumber = offset;

                        if(offset > 0)
                        {
                            lineNumber -= _totalPage;

                            for (var i = 0; i < offset - _totalPage; i++)
                            {
                                if (await sr.ReadLineAsync() == null)
                                {
                                    break;
                                }
                            }
                        }
                        
                        while ((line = await sr.ReadLineAsync()) != null && buffer.Count < _total + _totalPage)
                        {
                            lineNumber++;
                            buffer.Add(lineNumber, line);
                        }

                        EndOfFile = sr.EndOfStream;
                    }

                    if (buffer.Count < _total && !EndOfFile)
                    {
                        var rest = _total - buffer.Count;
                        buffer = await GetBuffer(offset - rest);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
                Console.WriteLine(string.Format(" O arquivo {0} não foi localizado!", FilePath));
            }

            return buffer;
        }
    }
}
