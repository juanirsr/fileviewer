﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileViewer.Services.FileReader.Interfaces
{
    public interface IFileReaderService
    {
        string FilePath { get; set; }
        bool EndOfFile { get; set; }

        Task<Dictionary<int, string>> GetBuffer(int offset = 0);
    }
}
