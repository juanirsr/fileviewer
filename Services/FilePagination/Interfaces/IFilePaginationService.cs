﻿using System.Threading.Tasks;

namespace FileViewer.Services.FilePagination.Interfaces
{
    public interface IFilePaginationService
    {
       Task WaitPressKey();
    }
}
