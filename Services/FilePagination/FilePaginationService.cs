﻿using FileViewer.Core.Enum;
using FileViewer.Services.FilePagination.Interfaces;
using FileViewer.Services.FileReader;
using FileViewer.Services.FileReader.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileViewer.Services.FilePagination
{
    public class FilePaginationService : IFilePaginationService
    {
        const int _totalPage = 11;
        const int _offset = 100;
        
        private int _arrow = 0;
        private int _offsetBuffer = 0;
        private Dictionary<int, string> _buffer;
        private readonly IFileReaderService _fileReaderService;

        public FilePaginationService(string path)
        {
            _fileReaderService = new FileReaderService();
            _fileReaderService.FilePath = path;

            InitialPage();
        }
        public async Task WaitPressKey()
        {
            while (true)
            {
                var result = Console.ReadKey();

                switch (result.Key.ToString())
                {
                    case "UpArrow":
                        await PressUpArrow(KeyTypes.UpArrow);
                        break;
                    case "DownArrow":
                        await PressDownArrow(KeyTypes.DownArrow);
                        break;
                    case "PageUp":
                        await PressPageUp(KeyTypes.PageUp);
                        break;
                    case "PageDown":
                        await PressPageDown(KeyTypes.PageDown);
                        break;
                    case "L":
                        await PressLine();
                        break;
                    default:
                        break;
                }
            }
        }

        private async Task InitialPage()
        {
            _buffer = await _fileReaderService.GetBuffer();

            if (_buffer.Any())
            {
                foreach (var linha in _buffer.Take(_totalPage))
                {
                    Console.WriteLine(linha.Key + " - " + linha.Value);
                }
            }
            else
            {
                Console.WriteLine("O arquivo está vazio.");
            }
        }

        private async Task PressUpArrow(KeyTypes keyType)
        {
            if(_arrow > 0)
            {
                _arrow--;
                
                var localBuffer = await GetLocalBuffer(keyType, _arrow);
                Write(localBuffer);
            }
        }

        private async Task PressDownArrow(KeyTypes keyType)
        {
            _arrow++;
            
            var localBuffer = await GetLocalBuffer(keyType, _arrow);
            Write(localBuffer);
        }

        private async Task PressPageUp(KeyTypes keyType)
        {
            var point = _arrow - _totalPage;
            _arrow = point;

            if (point < 0)
            {
                point = 0;
                _arrow = 0;
            }
            Write(await GetLocalBuffer(keyType, point));
        }

        private async Task PressPageDown(KeyTypes keyType)
        {
            var point = _arrow + _totalPage;
            _arrow = point;
            Write(await GetLocalBuffer(keyType, point));
        }

        private async Task PressLine()
        {
            Console.WriteLine("Digite a linha:");
            var line = Console.ReadLine();

            int lineNumber = 0;

            try
            {
                lineNumber = int.Parse(line);

                if (lineNumber > 0)
                {
                    var localBuffer = await GetLocalBufferByLine(lineNumber);
                    Write(localBuffer);
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Número de linha inválido!");
            }
        }
        
        private async Task<IEnumerable<KeyValuePair<int, string>>> GetLocalBuffer(KeyTypes keyType, int point)
        {
            var tryCount = 0;
            
            var localBuffer = _buffer.Where(l => l.Key > point && l.Key <= _totalPage + point);

            while (localBuffer.Count() < _totalPage)
            {
                if(tryCount == 0)
                {
                    await GetNewBuffer(keyType);
                }
                else if(tryCount == 1)
                {
                    point = _buffer.Last().Key - _totalPage < 0 ? 0 : _buffer.Last().Key - _totalPage;
                    _arrow = point;
                }
                else if(tryCount > 1)
                {
                    break;
                } 

                localBuffer = _buffer.Where(l => l.Key > point && l.Key <= _totalPage + point);

                tryCount++;
            }

            return localBuffer;
        }

        private async Task<IEnumerable<KeyValuePair<int, string>>> GetLocalBufferByLine(int lineNumber)
        {
            IEnumerable<KeyValuePair<int, string>> localBuffer = null;

            var minLineNumber = (lineNumber - 5 < 1) ? 1 : lineNumber - 5;
            var maxLineNumber = lineNumber + 5;

            var buffer = await _fileReaderService.GetBuffer(CalculationOffset(lineNumber));

            if (buffer.Any(l => l.Key == lineNumber))
            {
                if (buffer.Last().Key == lineNumber)
                {
                    buffer = await _fileReaderService.GetBuffer(CalculationOffset(lineNumber + 5));
                }

                localBuffer = buffer.Where(l => l.Key >= minLineNumber && l.Key <= maxLineNumber);
                var tryCount = 0;

                while (localBuffer.Count() < _totalPage)
                {
                    if (tryCount == 0)
                    {
                        var lastLine = localBuffer.Last().Key;

                        if (lineNumber - 5 < 1)
                        {
                            maxLineNumber += 5 - (lineNumber - 1);
                        }
                        else if (lastLine != maxLineNumber)
                        {
                            minLineNumber -= maxLineNumber - lastLine;
                        }
                    }
                    else if (tryCount > 0)
                    {
                        break;
                    }

                    localBuffer = buffer.Where(l => l.Key >= minLineNumber && l.Key <= maxLineNumber).Take(_totalPage);
                    tryCount++;
                }

                _buffer = buffer;
                _arrow = localBuffer.First().Key - 1;
            }
            else
            {
                Console.WriteLine("Número de linha inexistente!");
            }

            return localBuffer;
        }

        private int CalculationOffset(int lineNumber)
        {
            double pageCalculation = (Convert.ToDouble(lineNumber) / Convert.ToDouble(_offset));
            int page = int.Parse(Math.Ceiling(pageCalculation).ToString());
            return (page - 1) * _offset;
        }

        private void Write(IEnumerable<KeyValuePair<int, string>> localBuffer)
        {
            if (!(localBuffer is null))
            {
                Console.Clear();

                foreach (var linha in localBuffer)
                {
                    Console.WriteLine(linha.Key + " - " + linha.Value);
                }
            }
        }

        private async Task GetNewBuffer(KeyTypes keyType) 
        {
            if(keyType == KeyTypes.DownArrow || keyType == KeyTypes.PageDown)
            {
                if (!_fileReaderService.EndOfFile)
                {
                    _offsetBuffer += _offset;
                }
            }
            else
            {
                _offsetBuffer -= ((_offsetBuffer - _offset) < 0) ? 0 : _offset;
            }
           
            var buffer = await _fileReaderService.GetBuffer(_offsetBuffer);

            if(buffer.Any())
            {
                if (buffer.Last().Key == _buffer.Last().Key)
                {
                    _offsetBuffer -= ((_offsetBuffer -_offset) < 0) ? 0 : _offset;
                }

                _buffer = buffer;
            }
        }
    }
}
